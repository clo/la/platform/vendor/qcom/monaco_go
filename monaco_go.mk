
##############################Go configs###########################################
#Go variant flag
TARGET_HAS_LOW_RAM := true
PRODUCT_PROPERTY_OVERRIDES += ro.config.low_ram=true
PRODUCT_PROPERTY_OVERRIDES += ro.config.ringtone=Ring_Synth_04.ogg

# Enable DM file preopting to reduce first boot time
PRODUCT_DEX_PREOPT_GENERATE_DM_FILES := true
PRODUCT_DEX_PREOPT_DEFAULT_COMPILER_FILTER := verify

DONT_UNCOMPRESS_PRIV_APPS_DEXS := true

#target specific runtime prop for qspm
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.qspm.enable=true

# Reduces GC frequency of foreground apps by 50%
PRODUCT_PROPERTY_OVERRIDES += dalvik.vm.foreground-heap-growth-multiplier=2.0
PRODUCT_MINIMIZE_JAVA_DEBUG_INFO := true

# Disable per_app memcg
PRODUCT_PROPERTY_OVERRIDES += ro.config.per_app_memcg=false

PRODUCT_PACKAGE_OVERLAYS := device/qcom/monaco_go/overlay-go

PRODUCT_PACKAGES += \
    android.hardware.cas@1.2-service-lazy

PRODUCT_PACKAGES += \
    android.hardware.sensors@2.1-service.multihal \
    android.hardware.sensors@2.0-ScopedWakelock

PRODUCT_PROPERTY_OVERRIDES += persist.vendor.sensors.enable.bypass_worker=true
#Enable WearQSTA
PRODUCT_PROPERTY_OVERRIDES += ro.vendor.sensors.wearqstp=1
#WearQSTA Wakelock by default disabled
PRODUCT_PROPERTY_OVERRIDES += ro.vendor.sensors.wearqstp.lock=0

# Configstore is disabled for Android Go targets
PRODUCT_PACKAGES += disable_configstore

$(call inherit-product, build/target/product/go_defaults.mk)
$(call inherit-product-if-exists, frameworks/base/data/sounds/AudioPackageGo.mk)
#########################End of Go configs########################################

####################Wearable flags Start###########################
BOARD_COMMON_DIR := device/qcom/common
BOARD_SEPOLICY_DIR := device/qcom/sepolicy
BOARD_DLKM_DIR := device/qcom/common/dlkm
BOARD_DISPLAY_HAL := hardware/qcom/display
BOARD_BT_DIR := hardware/qcom/bt
BOARD_OPENSOURCE_DIR := vendor/qcom/opensource
TARGET_SUPPORTS_WEARABLES := true
TARGET_SUPPORTS_WEAR_OS := false
TARGET_SUPPORTS_WEAR_ANDROID := true
TARGET_SUPPORTS_WEAR_AON := false
AUDIO_FEATURE_ENABLED_SPLIT_A2DP := true
TARGET_SYSTEM_PROP := device/qcom/monaco_go/system.prop
BOARD_HAVE_QCOM_FM := false
TARGET_DISABLE_PERF_OPTIMIATIONS := false

TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

# Enable incremental fs
PRODUCT_PROPERTY_OVERRIDES += \
     ro.incremental.enable=yes

# Flag to enable DeepSleep
TARGET_SUPPORTS_DS := true

# privapp-permissions whitelisting (To Fix CTS :privappPermissionsMustBeEnforced)
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

$(call inherit-product, device/qcom/common/common.mk)

# Set hardware type to watch
PRODUCT_COPY_FILES += \
    device/qcom/monaco_go/wearable_core_hardware.xml:system/etc/permissions/wearable_core_hardware.xml
PRODUCT_CHARACTERISTICS := nosdcard,watch

PRODUCT_COPY_FILES += \
device/qcom/common/wearable_excluded_core_hardware.xml:system/etc/permissions/wearable_excluded_core_hardware.xml

#####################Wearable flags End############################

PRODUCT_COPY_FILES += \
    frameworks/base/data/sounds/effects/camera_click.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/camera_click.ogg

# Temporary bring-up config -->
ALLOW_MISSING_DEPENDENCIES := true

# Enable AVB 2.0
BOARD_AVB_ENABLE := true

# Default A/B configuration
ENABLE_AB := true

# Enable virtual-ab by default
ENABLE_VIRTUAL_AB := true
ifeq ($(ENABLE_VIRTUAL_AB), true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif

$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)

# Enable Dynamic partition
BOARD_DYNAMIC_PARTITION_ENABLE ?= true

# Set API Level for R
SHIPPING_API_LEVEL ?= 30
PRODUCT_SHIPPING_API_LEVEL := $(SHIPPING_API_LEVEL)

#Suppot to compile recovery without msm headers
TARGET_HAS_GENERIC_KERNEL_HEADERS := true

# Include mainline components
PRODUCT_ENFORCE_ARTIFACT_PATH_REQUIREMENTS := true

#Enable product partition Native I/F. It is automatically set to current if
#the shipping API level for the target is greater than 29
PRODUCT_PRODUCT_VNDK_VERSION := current

#Enable product partition Java I/F. It is automatically set to true if
#the shipping API level for the target is greater than 29
PRODUCT_ENFORCE_PRODUCT_PARTITION_INTERFACE := true

ifeq ($(ENABLE_AB), true)
PRODUCT_BUILD_CACHE_IMAGE := false
else
PRODUCT_BUILD_CACHE_IMAGE := true
endif
PRODUCT_BUILD_RAMDISK_IMAGE := true
PRODUCT_BUILD_USERDATA_IMAGE := true

ifeq ($(strip $(BOARD_DYNAMIC_PARTITION_ENABLE)),true)
PRODUCT_USE_DYNAMIC_PARTITIONS := true
# Enable product partition
PRODUCT_BUILD_PRODUCT_IMAGE := true
# Enable System_ext
PRODUCT_BUILD_SYSTEM_EXT_IMAGE := true

PRODUCT_PACKAGES += fastbootd
# Add default implementation of fastboot HAL.
PRODUCT_PACKAGES += android.hardware.fastboot@1.0-impl-mock

# enable vbmeta_system
BOARD_AVB_VBMETA_SYSTEM := system product system_ext
BOARD_AVB_VBMETA_SYSTEM_KEY_PATH := external/avb/test/data/testkey_rsa2048.pem
BOARD_AVB_VBMETA_SYSTEM_ALGORITHM := SHA256_RSA2048
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX := $(PLATFORM_SECURITY_PATCH_TIMESTAMP)
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX_LOCATION := 2
$(call inherit-product, build/make/target/product/gsi_keys.mk)
endif
# f2fs utilities
 PRODUCT_PACKAGES += \
     sg_write_buffer \
     f2fs_io \
     check_f2fs

 # Userdata checkpoint
 PRODUCT_PACKAGES += \
     checkpoint_gc

 ifeq ($(ENABLE_AB), true)
  AB_OTA_POSTINSTALL_CONFIG += \
      RUN_POSTINSTALL_vendor=true \
      POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
      FILESYSTEM_TYPE_vendor=ext4 \
      POSTINSTALL_OPTIONAL_vendor=true
   PRODUCT_COPY_FILES += $(LOCAL_PATH)/fstab_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom
 else
   PRODUCT_COPY_FILES += $(LOCAL_PATH)/fstab_non_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom
 endif

#fstab.qcom
PRODUCT_PACKAGES += fstab.qcom

# Temporary bring-up config -->
PRODUCT_SUPPORTS_VERITY := false
# Temporary bring-up config <--
###########
PRODUCT_PROPERTY_OVERRIDES  += \
     dalvik.vm.heapstartsize=8m \
     dalvik.vm.heapsize=512m \
     dalvik.vm.heaptargetutilization=0.75 \
     dalvik.vm.heapminfree=512k \
     dalvik.vm.heapmaxfree=8m
# Target naming
PRODUCT_NAME := monaco_go
PRODUCT_DEVICE := monaco_go
PRODUCT_AAPT_CONFIG := mdpi
PRODUCT_BRAND := qti
PRODUCT_MODEL := Monaco for arm


TARGET_USES_AOSP := false
TARGET_USES_AOSP_FOR_AUDIO := false
TARGET_USES_QCOM_BSP := false

ifeq ($(TARGET_USES_AOSP),true)
TARGET_DISABLE_DASH := true
endif

#Enable SecureElement to support  on LAW.UM.2.0
ifeq ($(TARGET_SUPPORTS_WEARABLES),true)
PRODUCT_PACKAGES += SecureElement
endif

# diag-router
TARGET_HAS_DIAG_ROUTER := true

#Binder size property
PRODUCT_PROPERTY_OVERRIDES += \
    vendor.mediacodec.binder.size=256

TARGET_DISABLE_DISPLAY := false

# Kernel configurations
ifneq ($(wildcard kernel/msm-4.19),)
    TARGET_KERNEL_VERSION := 4.19
    $(warning "Build with 4.19 kernel.")
else ifneq ($(wildcard kernel/msm-5.4),)
    TARGET_KERNEL_VERSION := 5.4
    $(warning "Build with 5.4 kernel.")
else
    $(warning "Build with unknown kernel.")
endif

ifeq ($(TARGET_KERNEL_VERSION), 5.4)
TARGET_USES_5.4_KERNEL := true
endif

#Enable llvm support for kernel
KERNEL_LLVM_SUPPORT := true

#Enable aosp-llvm support for kernel 5.4 compilation
ifeq ($(TARGET_USES_5.4_KERNEL), true)
KERNEL_SD_LLVM_SUPPORT := false
else
KERNEL_SD_LLVM_SUPPORT := true
endif

#disable dpp project
ifeq ($(TARGET_USES_5.4_KERNEL), true)
TARGET_EXCLUDES_DISPLAY_PP := true
endif

###########
# Target configurations

QCOM_BOARD_PLATFORMS += monaco

#Default vendor image configuration
ENABLE_VENDOR_IMAGE := true

# default is nosdcard, S/W button enabled in resource
PRODUCT_CHARACTERISTICS := nosdcard

BOARD_FRP_PARTITION_NAME := frp

# Android EGL implementation
PRODUCT_PACKAGES += libGLES_android

PRODUCT_PACKAGES += fs_config_files
PRODUCT_PACKAGES += gpio-keys.kl
PRODUCT_PACKAGES += qpnp_pon.kl
PRODUCT_PACKAGES += libvolumelistener

ifeq ($(ENABLE_AB), true)
# A/B related packages
PRODUCT_PACKAGES += update_engine \
    update_engine_client \
    update_verifier \
    android.hardware.boot@1.1-impl-qti \
    android.hardware.boot@1.1-impl-qti.recovery \
    android.hardware.boot@1.1-service

PRODUCT_HOST_PACKAGES += \
    brillo_update_payload
# Boot control HAL test app
PRODUCT_PACKAGES_DEBUG += bootctl

PRODUCT_PACKAGES += \
  update_engine_sideload

endif
DEVICE_FRAMEWORK_MANIFEST_FILE := device/qcom/monaco_go/framework_manifest.xml

DEVICE_MANIFEST_FILE := device/qcom/monaco_go/manifest.xml
ifeq ($(ENABLE_AB), true)
DEVICE_MANIFEST_FILE += device/qcom/monaco_go/manifest_ab.xml
endif
DEVICE_MATRIX_FILE   := device/qcom/common/compatibility_matrix.xml
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
    vendor/qcom/opensource/core-utils/vendor_framework_compatibility_matrix.xml \
    device/qcom/monaco_go/vendor_framework_compatibility_matrix.xml

# Kernel modules install path
KERNEL_MODULES_INSTALL := dlkm
KERNEL_MODULES_OUT := out/target/product/$(PRODUCT_NAME)/$(KERNEL_MODULES_INSTALL)/lib/modules

# system prop for opengles version
#
# 196608 is decimal for 0x30000 to report version 3
# 196609 is decimal for 0x30001 to report version 3.1
# 196610 is decimal for 0x30002 to report version 3.2
PRODUCT_PROPERTY_OVERRIDES  += \
    ro.opengles.version=196609

# Audio QSSI configuration file
$(call inherit-product-if-exists, vendor/qcom/opensource/commonsys/audio/configs/qssi/qssi.mk)

# Audio configuration file
-include $(TOPDIR)vendor/qcom/opensource/audio-hal/primary-hal/configs/monaco/monaco.mk

# Enable telephpony ims feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.telephony.ims.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.telephony.ims.xml



#Enable full treble flag
PRODUCT_FULL_TREBLE_OVERRIDE := true
PRODUCT_VENDOR_MOVE_ENABLED := true
PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true
BOARD_VNDK_VERSION := current
TARGET_MOUNT_POINTS_SYMLINKS := false

PRODUCT_BOOT_JARS += telephony-ext
PRODUCT_PACKAGES += telephony-ext

PRODUCT_BOOT_JARS += tcmiface

# Defined the locales
PRODUCT_LOCALES := en_US

# Vendor property to enable advanced network scanning
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.radio.enableadvancedscan=true

# Property to disable ZSL mode
PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=1

PRODUCT_PROPERTY_OVERRIDES += \
ro.crypto.volume.filenames_mode = "aes-256-cts" \
ro.crypto.allow_encrypt_override = true

#namespace definition for software omx plugin
SOONG_CONFIG_NAMESPACES += soft_omx
SOONG_CONFIG_soft_omx += disable_soft_omx_flac_dec
SOONG_CONFIG_soft_omx_disable_soft_omx_flac_dec := true

#----------------------------------------------------------------------
# wlan specific
#----------------------------------------------------------------------
include device/qcom/wlan/monaco_go/wlan.mk

###################################################################################
# This is the End of target.mk file.
# Now, Pickup other split product.mk files:
###################################################################################
# TODO: Relocate the system product.mk files pickup into qssi lunch, once it is up.
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/system/*.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/*.mk)
###################################################################################

#############include power make files##############################################
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/power-vendor-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/legacy/power-vendor-board.mk)
###################################################################################

#############include BT make files#################################################
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-system-opensource-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-system-proprietary-product)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-vendor-proprietary-product.mk)
###################################################################################

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
ro.vendor.qc_aon_presence = 0

RIL_USE_ADB_PROP_FOR_APM_SIM_NOT_PWDN := true
PRODUCT_PROPERTY_OVERRIDES += persist.vendor.radio.apm_sim_not_pwdn=0
